#ifndef STATUS_ENUM_H
#define STATUS_ENUM_H

enum status {
    SUCCESS = 0,
    FILE_OPENING_ERROR,
    FILE_CLOSING_ERROR,
    READING_ERROR,
    WRITING_ERROR,
};

#endif











