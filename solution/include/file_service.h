#ifndef FILE_SERVICE_H
#define FILE_SERVICE_H


#include "status_enum.h"
#include <bits/types/FILE.h>

enum status file_open(const char *file_name, FILE **file, char *mode);

enum status file_close(FILE *file);

#endif
