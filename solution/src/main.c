#include "../include/image.h"
#include "../include/bmp_format.h"
#include "../include/file_service.h"
#include "../include/status_enum.h"
#include "../include/transformation.h"
#include <stdio.h>

int main(int argc, char **argv) {
    
    char *status_info[] = {
        [SUCCESS] = "Картинка успешно перевернута",
        [FILE_OPENING_ERROR] = "Не удалось открыть файл",
        [FILE_CLOSING_ERROR] = "Не удалось закрыть файл",
        [READING_ERROR] = "Не удалось прочитать из файла",
        [WRITING_ERROR] = "Не удалось записать в файл"
	};
 
    if (argc != 3) {
        printf("Неверное количество аргументов\n");
        return 0;
    }

    FILE *input_file = {0};
    struct image first_image = {0};
    FILE *output_file = {0};
    
    const char* input_file_name = argv[1];
    const char* output_file_name = argv[2];

    enum status status = file_open(input_file_name, &input_file, "r");
    if (status != SUCCESS) printf("%s\n",status_info[status]);

    status = from_bmp(input_file, &first_image);
    if (status != SUCCESS) printf("%s\n",status_info[status]);

    status = file_close(input_file);
    if (status != SUCCESS) printf("%s\n",status_info[status]);

    struct image second_image = transform(&first_image);
    image_destroy(first_image);

    status = file_open(output_file_name, &output_file, "w");
    if (status != SUCCESS) printf("%s\n",status_info[status]);

    status = to_bmp(output_file, &second_image);
    if (status != SUCCESS) printf("%s\n",status_info[status]);

    status = file_close(output_file);
    if (status != SUCCESS) printf("%s\n",status_info[status]);

    image_destroy(second_image);
    printf("%s\n",status_info[SUCCESS]);

    return 0;
}
