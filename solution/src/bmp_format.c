#include "../include/bmp_format.h"
#include "../include/bmp_header.h"
#include "stdint.h"
#include <stdbool.h>



static uint32_t BFTYPE = 19778;
static uint32_t RESERVED = 0;
static uint32_t HEADER_SIZE = 40;
static uint16_t PLANES = 1;
static uint32_t COMPRESSION = 0;
static uint32_t PIXEL_PER_METER = 0;
static uint32_t COLORS_USED = 0;
static uint32_t COLORS_IMPORTANT = 0;
static size_t BIT_COUNT = 24;



bool read_header(FILE *file, struct bmp_header *header) {
    return fread(header, sizeof(struct bmp_header), 1, file);
}

size_t size_of_padding(const size_t width) {
    if (width % 4 == 0) return 0;
    return 4 - ((width * 3) % 4);
}

size_t size_of_image(const struct image *image) {
    return image->height * (3 * image->width + size_of_padding(image->width));
}

size_t size_of_file(const size_t image_size) {
    return image_size + sizeof(struct bmp_header);
}

enum status from_bmp(FILE *in, struct image *image) {
    struct bmp_header header = {0};
    if (!read_header(in, &header)) {
        return READING_ERROR;
    }

    *image = image_create(header.biWidth, header.biHeight);

    size_t i = 0;
    size_t j = 0;
    
    while (i < image->height){
    	while (j < image->width){
    		fread(&(image->data[image->width * i + j]), sizeof(struct pixel), 1, in);
    		j++;
    	}
    	if(fseek(in, (long) size_of_padding(image->width), SEEK_CUR)!=0){
    		return READING_ERROR;
    	}
    	i++;
    	j=0;
    }
    
    return SUCCESS;
}

enum status to_bmp(FILE *out, const struct image *image) {

    struct bmp_header header = {0};
   
    header.bfType = BFTYPE;
    header.bfileSize = size_of_file(size_of_image(image));
    header.bfReserved = RESERVED;
    header.bOffBits = sizeof(struct bmp_header);
    header.biSize = HEADER_SIZE;
    header.biWidth = image->width;
    header.biHeight = image->height;
    header.biPlanes = PLANES;
    header.biBitCount = BIT_COUNT;
    header.biCompression = COMPRESSION;
    header.biSizeImage = size_of_image(image);
    header.biXPelsPerMeter = PIXEL_PER_METER;
    header.biYPelsPerMeter = PIXEL_PER_METER;
    header.biClrUsed = COLORS_USED;
    header.biClrImportant = COLORS_IMPORTANT;
	
    if (!fwrite(&header, sizeof(struct bmp_header), 1, out)) {
        return WRITING_ERROR;
    }

    if(fseek(out, header.bOffBits, SEEK_SET)!=0){
    	return WRITING_ERROR;
    }

    const uint8_t zero = 0;
    
    size_t i = 0;
    size_t j = 0;
    
    if (image->data) {
        while (i < image->height){
        	if (!fwrite(image->data + i * image->width, image->width * sizeof(struct pixel), 1, out)) return WRITING_ERROR;
        	while (j < size_of_padding(image->width)){
        		if (!fwrite(&zero, 1, 1, out)) return WRITING_ERROR;
        		j++;
        	}
       	i++;
        	j=0;
        }      
    } else {
        return WRITING_ERROR;
    }

    return SUCCESS;
}


