#include "../include/file_service.h"
#include <errno.h>
#include <stdio.h>
#include <string.h>

enum status file_open(const char *file_name, FILE **file, char *mode) {

    *file = fopen(file_name, mode);
    if (*file == NULL) {
        return FILE_OPENING_ERROR;
    }

    return SUCCESS;
}

enum status file_close(FILE *file) {
    if (fclose(file) == EOF) {
        return FILE_CLOSING_ERROR;
    }
    return SUCCESS;
}
